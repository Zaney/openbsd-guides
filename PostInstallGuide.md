# Complete Post Install Guide For OpenBSD

This guide is written as though you are wanting to clone and get my exact setup. However, this is not required. Plenty of the things covered here will be great information for any regular install. If you would like to use my system as a starting point then this guide is perfect.

## Prelude

So even though this is a post install guide we need to cover something that will become relevant later. If you're unfamiliar with drives on OpenBSD, anything over 356 GB will be left as unused. So during the install instead of just using the regular auto partitioning scheme, I use the E (Edit) option. 

I bump up some of the automatic partitions just to make me feel safer over the long term. Here is some of the commands I use.

For printing in gigabytes, on God - why is this not the default. Like fuck me who is contantly working in bytes??

```
p g
```

To resize one of the automatic partitions.

```
R [part]
```
*for size you can enter something like 20g for 20 GB*

For adding partition (leave no mount point we'll do it later).

```
a
```

To write and quit saving changes.

```
w
q
```

## First Boot After Install

Now this is also assuming you made a regular user during install. If you didn't this is where you should take care of that. 

Firstly, log in as root. Then run:

```
syspatch
```

Then install some much needed programs.

```
pkg_add vim neofetch rust git curl wget
```

### Adding Regular User Permissions

Then use Vim to edit your */etc/doas.conf* so your user can have permissions. In my case this is tyler:

permit persist tyler as root

### Dotfile Manager

Use curl to pull down yadm (Yet Another Dotfiles Manager), this is what I use to manage my dotfiles. It's remarkably easy to use, and just simplifies things.

```
curl -fLo /usr/local/bin/yadm https://github.com/TheLocehiliosan/yadm/raw/master/yadm && chmod a+x /usr/local/bin/yadm
```

Then remove conflicting .profile & clone my dotfiles using yadm:

```
rm .profile
yadm clone https://gitlab.com/Zaney/dotfiles.git
```

After that do a reboot so we get all the new patched kernel and stuff. As well as when you boot up this time, log in as your user and you should notice a different colorized ksh prompt and an error about pokemon-colorscripts. (We will fix the error in a bit, you will also get some errors when opening Vim - it will still work).

## Install All The Programs

Now that we are our regular user let's get lsd, no not the drug, ls deluxe makes the ls command just better. This is what I like to use, it's a rust program so you can simply install it like so:

```
cargo install lsd
```

Now let's build both dwm and st. 

```
cd ~/.config/suckless/dwm
doas make clean install
..
cd st/
doas make clean install
cd
```

Now let's install pretty much every single program I need...

```
doas pkg_add rofi gimp kdenlive fff mpv xdotool xclip scrot lxappearance chromium weechat feh dunst unzip audacity openra 0ad xonotic supertuxkart cmatrix cmus hermit-font symbola-ttf
```

Then we will want to run fc-cache to make sure our fonts get updated.

## Setting OpenBSD System Settings

Now we will want to set all the settings for OpenBSD itself that we need. This is done through two files. sysctl.conf and login.conf - for the first we set some important values for Chrome/Firefox as they like to eat up resources. You will use /etc/sysctl.conf to set kernel options from time to time. 

The /etc/login.conf is where we set more modern values for our staff group. We will need to be in this group for our bumped up settings to affect us.

So we will copy the etc/ folder files indkkividually to their proper location making sure to change values as needed for your system.

```
vim etc/login.conf
doas rm /etc/login.conf
doas cp etc/login.conf /etc/login.conf
vim etc/sysctl.conf
doas cp etc/sysctl.conf /etc/sysctl.conf
```

Now add our user to the staff group so we can take advantage of more system resources. Remember for my system the username is tyler, change appropriately.

```
doas usermod -G staff tyler
```

### Edit /etc/fstab For Better Performance

Now we can edit our fstab to increase performance with our drive. We need to add softdep and noatime to our partition list, like so:

```
0364c44477d30004.b none swap sw
0364c44477d30004.a / ffs rw,softdep,noatime 1 1
0364c44477d30004.l /home ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.d /tmp ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.f /usr ffs rw,softdep,noatime,nodev 1 2
0364c44477d30004.g /usr/X11R6 ffs rw,softdep,noatime,nodev 1 2
0364c44477d30004.h /usr/local ffs rw,softdep,noatime,wxallowed,nodev 1 2
0364c44477d30004.k /usr/obj ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.j /usr/src ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.e /var ffs rw,softdep,noatime,nodev,nosuid 1 2
```

### Format Any Extra Partitions & Add Them

Now if you are doing your install anything like I am you will have some extra partitions that need to be formatted and then added to the fstab. You can list the partitions for a drive like so:

```
doas disklabel <sdX>
```

Format the partiitions with this command for each partition letter that you added.

```
newfs rsnd<X>
```

Add them to your fstab like so(change tyler to your user):

```
0364c44477d30004.b none swap sw
0364c44477d30004.a / ffs rw,softdep,noatime 1 1
0364c44477d30004.l /home ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.m /home/tyler/Videos ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.n /home/tyler/Music ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.d /tmp ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.f /usr ffs rw,softdep,noatime,nodev 1 2
0364c44477d30004.g /usr/X11R6 ffs rw,softdep,noatime,nodev 1 2
0364c44477d30004.h /usr/local ffs rw,softdep,noatime,wxallowed,nodev 1 2
0364c44477d30004.k /usr/obj ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.j /usr/src ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.e /var ffs rw,softdep,noatime,nodev,nosuid 1 2
```

Now we can add the folders to our home directory. And go ahead and mount the drives now.

```
cd
mkdir Videos
mkdir Music
doas mount /dev/sd<X>m Videos/
doas mount /dev/sd<X>n Music/
```

## Install Compositor & Fix Vim

Before we move into installing the compositor, real quickly we will install pokemon-colorscripts so we will get our great looking pokemon with each terminal we open.

```
git clone https://gitlab.com/Zaney/pokemon-colorscripts.git
cd pokemon-colorscripts
doas ./install.sh
cd ..
rm -rf pokemon-colorscripts
```

Next we will install the custom version of picom I have in my [Gitlab](https://gitlab.com/Zaney/picom). Follow the instructions there to install, make sure to install the dependencies!

### Let's Fix That Annoying Vim Warning

When you open anything with Vim up to this point you will have noticed the warnings/errors that it spits out. That is becuase I have plugins in the .vimrc file that we can't use without Vim-Plug. Let's install it and then install the plugins.

```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

Now open the .vimrc in the HOME directory and type *:PlugInstall* and enter. Vim should start installing the plugins I use. Once it's done you can close out of Vim. You now have my Vim setup.

Go ahead and do a reboot after all this to make sure drives mount correctly after you changed them a minute ago. If all goes as planned continue on when you are rebooted and logged in as your user.

## The Final Test

Now with you logged in and with everything setup the way I would have it on my system try running startx. This will use my .xinitrc which you should definitely take a look at. At the very least to see what I use at startup.

If all goes as intended, you should see dwm with a wallpaper of Puffy the OpenBSD logo front and center. At this point you have my system setup pretty much. To add the cherry on top I recommend you go to the chrome web store and grab Dark Reader. When you click on it, you may need to choose dev tools and click the button with the text "Preview New Design." After clicking this you should have color and options settings that you can set. Please watch my video [here](https://youtu.be/eIabbF2G4Qg) to have your chrome match the rest of the desktop! 
