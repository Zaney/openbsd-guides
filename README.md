# OpenBSD Guides

This repository is meant to be a collection of OpenBSD guides handwritten by myself that will hopefully grow to encompass much of what new comers will want to know. This is in no way intended to replace the fantastic documentation of OpenBSD or the FAQ. Those are incredible resources and truly need no expansion. 

These guides are here to provide simple step-by-step guidence to anyone that should want / need it. These are here as merely a fallback so that anyone new can quickly follow one of these to address a situation in a pinch. Hope they help you!

## Guides

- [Managing Drives](ManagingDrives.md)
- [OpenBSD Post Install Guide](PostInstallGuide.md)
